#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>
#include <string>
#include <QProcess>

namespace ffmpeg{
    extern "C"
    {
    #include "avcodec.h"
    #include "avformat.h"
    #include "swscale.h"
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_toolButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
    "",
    tr(""));
    ui->fileInfo->setText(fileName);
}

void MainWindow::on_toolButton_2_clicked()
{
    QString saveName = QFileDialog::getSaveFileName(this, tr("Open File"),
    "",
    tr(""));
    ui->saveInfo->setText(saveName);
}

int MainWindow::on_startHour_textChanged(const QString &arg1)
{
    QString startHourTest = ui->startHour->text();
    int startHourNum = arg1.toInt();
    return startHourNum;

}

int MainWindow::on_startMinute_textChanged(const QString &arg1)
{
    QString startMinText = ui->startMinute->text();
    int startMinNum = arg1.toInt();
    return startMinNum;
}

double MainWindow::on_startSeconds_textChanged(const QString &arg1)
{
    QString startSecText = ui->startSeconds->text();
    double startSecNum = arg1.toDouble();
    return startSecNum;
}

int MainWindow::on_endHour_textChanged(const QString &arg1)
{
    QString endHourText = ui->endHour->text();
    int endHourNum = arg1.toInt();
    return endHourNum;
}

int MainWindow::on_endMinute_textChanged(const QString &arg1)
{
    QString endMinText = ui->endMinute->text();
    int endMinNum = arg1.toInt();
    return endMinNum;
}

double MainWindow::on_endSeconds_textChanged(const QString &arg1)
{
    QString endSecText = ui->endSeconds->text();
    double endSecNum = arg1.toDouble();
    return endSecNum;
}

void MainWindow::on_pushButton_clicked()
{
    ffmpeg::avcodec_register_all();

    int begH = 60 * 60 * (MainWindow::on_startHour_textChanged(ui->startHour->text()));
    int begM = 60 * (MainWindow::on_startMinute_textChanged(ui->startMinute->text()));
    double begS = (MainWindow::on_startSeconds_textChanged(ui->startSeconds->text()));
    double begT = begH + begM + begS;
    double keyFrameTime = 0;

    if(begT > 361)
    {
        keyFrameTime = begT - 120;
        begT = 120;
    }
    else
    {
        keyFrameTime = 0;
    }

    int hour = 60 * 60 * (MainWindow::on_endHour_textChanged(ui->endHour->text()) - MainWindow::on_startHour_textChanged(ui->startHour->text()));
    int minute = 60 * (MainWindow::on_endMinute_textChanged(ui->endMinute->text()) - MainWindow::on_startMinute_textChanged(ui->startMinute->text()));
    double seconds = (MainWindow::on_endSeconds_textChanged(ui->endSeconds->text()) - MainWindow::on_startSeconds_textChanged(ui->startSeconds->text()));
    double all = hour + minute + seconds;
    //QString s = QString::number(hour);
    //QString t = QString::number(minute);
    //QString u = QString::number(seconds);
    QString v = QString::number(all);
    QString w = ui->fileInfo->text();
    QString x = QString::number(begT);
    QString y = QString::number(keyFrameTime);
    QString z = ui->saveInfo->text();

    QProcess *FFMPEG;

    //QString command = QString("C:\\ProgramData\\chocolatey\\bin\\ffmpeg.exe -ss " + x + " -i \"" + w + "\" " + "-c copy -ss 120 -t " + v + " \"" + z + "\"");
    QString path = QDir::currentPath();
    QString command = QString(path + "\\ffmpeg.exe -ss " + y + " -i \"" + w + "\" " + "-c copy -ss " + x + " -t " + v + " \"" + z + "\"");
    //QString command = "ffmpeg.exe";
    ui->debugTest->setText(command);

    //QProcess::startDetached("cmd.exe", QStringList() << "/c" << "sleep 10");
    FFMPEG->startDetached(command);
}
