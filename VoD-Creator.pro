#-------------------------------------------------
#
# Project created by QtCreator 2016-08-03T13:02:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = VoD-Creator
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp

CONFIG+= static\
    staticlib

HEADERS  += mainwindow.h \
    externals/ffmpeg/include/libavcodec/avcodec.h \
    externals/ffmpeg/include/libavcodec/avdct.h \
    externals/ffmpeg/include/libavcodec/avfft.h \
    externals/ffmpeg/include/libavcodec/d3d11va.h \
    externals/ffmpeg/include/libavcodec/dirac.h \
    externals/ffmpeg/include/libavcodec/dv_profile.h \
    externals/ffmpeg/include/libavcodec/dxva2.h \
    externals/ffmpeg/include/libavcodec/jni.h \
    externals/ffmpeg/include/libavcodec/mediacodec.h \
    externals/ffmpeg/include/libavcodec/qsv.h \
    externals/ffmpeg/include/libavcodec/vaapi.h \
    externals/ffmpeg/include/libavcodec/vda.h \
    externals/ffmpeg/include/libavcodec/vdpau.h \
    externals/ffmpeg/include/libavcodec/version.h \
    externals/ffmpeg/include/libavcodec/videotoolbox.h \
    externals/ffmpeg/include/libavcodec/vorbis_parser.h \
    externals/ffmpeg/include/libavcodec/xvmc.h \
    externals/ffmpeg/include/libavdevice/avdevice.h \
    externals/ffmpeg/include/libavdevice/version.h \
    externals/ffmpeg/include/libavfilter/avfilter.h \
    externals/ffmpeg/include/libavfilter/avfiltergraph.h \
    externals/ffmpeg/include/libavfilter/buffersink.h \
    externals/ffmpeg/include/libavfilter/buffersrc.h \
    externals/ffmpeg/include/libavfilter/version.h \
    externals/ffmpeg/include/libavformat/avformat.h \
    externals/ffmpeg/include/libavformat/avio.h \
    externals/ffmpeg/include/libavformat/version.h \
    externals/ffmpeg/include/libavutil/adler32.h \
    externals/ffmpeg/include/libavutil/aes.h \
    externals/ffmpeg/include/libavutil/aes_ctr.h \
    externals/ffmpeg/include/libavutil/attributes.h \
    externals/ffmpeg/include/libavutil/audio_fifo.h \
    externals/ffmpeg/include/libavutil/avassert.h \
    externals/ffmpeg/include/libavutil/avconfig.h \
    externals/ffmpeg/include/libavutil/avstring.h \
    externals/ffmpeg/include/libavutil/avutil.h \
    externals/ffmpeg/include/libavutil/base64.h \
    externals/ffmpeg/include/libavutil/blowfish.h \
    externals/ffmpeg/include/libavutil/bprint.h \
    externals/ffmpeg/include/libavutil/bswap.h \
    externals/ffmpeg/include/libavutil/buffer.h \
    externals/ffmpeg/include/libavutil/camellia.h \
    externals/ffmpeg/include/libavutil/cast5.h \
    externals/ffmpeg/include/libavutil/channel_layout.h \
    externals/ffmpeg/include/libavutil/common.h \
    externals/ffmpeg/include/libavutil/cpu.h \
    externals/ffmpeg/include/libavutil/crc.h \
    externals/ffmpeg/include/libavutil/des.h \
    externals/ffmpeg/include/libavutil/dict.h \
    externals/ffmpeg/include/libavutil/display.h \
    externals/ffmpeg/include/libavutil/downmix_info.h \
    externals/ffmpeg/include/libavutil/error.h \
    externals/ffmpeg/include/libavutil/eval.h \
    externals/ffmpeg/include/libavutil/ffversion.h \
    externals/ffmpeg/include/libavutil/fifo.h \
    externals/ffmpeg/include/libavutil/file.h \
    externals/ffmpeg/include/libavutil/frame.h \
    externals/ffmpeg/include/libavutil/hash.h \
    externals/ffmpeg/include/libavutil/hmac.h \
    externals/ffmpeg/include/libavutil/hwcontext.h \
    externals/ffmpeg/include/libavutil/hwcontext_cuda.h \
    externals/ffmpeg/include/libavutil/hwcontext_dxva2.h \
    externals/ffmpeg/include/libavutil/hwcontext_vaapi.h \
    externals/ffmpeg/include/libavutil/hwcontext_vdpau.h \
    externals/ffmpeg/include/libavutil/imgutils.h \
    externals/ffmpeg/include/libavutil/intfloat.h \
    externals/ffmpeg/include/libavutil/intreadwrite.h \
    externals/ffmpeg/include/libavutil/lfg.h \
    externals/ffmpeg/include/libavutil/log.h \
    externals/ffmpeg/include/libavutil/lzo.h \
    externals/ffmpeg/include/libavutil/macros.h \
    externals/ffmpeg/include/libavutil/mastering_display_metadata.h \
    externals/ffmpeg/include/libavutil/mathematics.h \
    externals/ffmpeg/include/libavutil/md5.h \
    externals/ffmpeg/include/libavutil/mem.h \
    externals/ffmpeg/include/libavutil/motion_vector.h \
    externals/ffmpeg/include/libavutil/murmur3.h \
    externals/ffmpeg/include/libavutil/opt.h \
    externals/ffmpeg/include/libavutil/parseutils.h \
    externals/ffmpeg/include/libavutil/pixdesc.h \
    externals/ffmpeg/include/libavutil/pixelutils.h \
    externals/ffmpeg/include/libavutil/pixfmt.h \
    externals/ffmpeg/include/libavutil/random_seed.h \
    externals/ffmpeg/include/libavutil/rational.h \
    externals/ffmpeg/include/libavutil/rc4.h \
    externals/ffmpeg/include/libavutil/replaygain.h \
    externals/ffmpeg/include/libavutil/ripemd.h \
    externals/ffmpeg/include/libavutil/samplefmt.h \
    externals/ffmpeg/include/libavutil/sha.h \
    externals/ffmpeg/include/libavutil/sha512.h \
    externals/ffmpeg/include/libavutil/stereo3d.h \
    externals/ffmpeg/include/libavutil/tea.h \
    externals/ffmpeg/include/libavutil/threadmessage.h \
    externals/ffmpeg/include/libavutil/time.h \
    externals/ffmpeg/include/libavutil/timecode.h \
    externals/ffmpeg/include/libavutil/timestamp.h \
    externals/ffmpeg/include/libavutil/tree.h \
    externals/ffmpeg/include/libavutil/twofish.h \
    externals/ffmpeg/include/libavutil/version.h \
    externals/ffmpeg/include/libavutil/xtea.h \
    externals/ffmpeg/include/libpostproc/postprocess.h \
    externals/ffmpeg/include/libpostproc/version.h \
    externals/ffmpeg/include/libswresample/swresample.h \
    externals/ffmpeg/include/libswresample/version.h \
    externals/ffmpeg/include/libswscale/swscale.h \
    externals/ffmpeg/include/libswscale/version.h

FORMS    += mainwindow.ui

DISTFILES += \
    externals/ffmpeg/lib/avcodec.lib \
    externals/ffmpeg/lib/avdevice.lib \
    externals/ffmpeg/lib/avfilter.lib \
    externals/ffmpeg/lib/avformat.lib \
    externals/ffmpeg/lib/avutil.lib \
    externals/ffmpeg/lib/libavcodec.dll.a \
    externals/ffmpeg/lib/libavdevice.dll.a \
    externals/ffmpeg/lib/libavfilter.dll.a \
    externals/ffmpeg/lib/libavformat.dll.a \
    externals/ffmpeg/lib/libavutil.dll.a \
    externals/ffmpeg/lib/libpostproc.dll.a \
    externals/ffmpeg/lib/libswresample.dll.a \
    externals/ffmpeg/lib/libswscale.dll.a \
    externals/ffmpeg/lib/postproc.lib \
    externals/ffmpeg/lib/swresample.lib \
    externals/ffmpeg/lib/swscale.lib \
    externals/ffmpeg/lib/avcodec-57.def \
    externals/ffmpeg/lib/avdevice-57.def \
    externals/ffmpeg/lib/avfilter-6.def \
    externals/ffmpeg/lib/avformat-57.def \
    externals/ffmpeg/lib/avutil-55.def \
    externals/ffmpeg/lib/postproc-54.def \
    externals/ffmpeg/lib/swresample-2.def \
    externals/ffmpeg/lib/swscale-4.def \
    .gitignore \
    LICENSE

win32: LIBS += -L$$PWD/externals/ffmpeg/lib/ -lavcodec -lavformat -lswscale

INCLUDEPATH += \
    $$PWD/externals/ffmpeg/lib \
    $$PWD/externals/ffmpeg/include/ \
    $$PWD/externals/ffmpeg/include/libavcodec \
    $$PWD/externals/ffmpeg/include/libavdevice \
    $$PWD/externals/ffmpeg/include/libavfilter \
    $$PWD/externals/ffmpeg/include/libavformat \
    $$PWD/externals/ffmpeg/include/libavutil \
    $$PWD/externals/ffmpeg/include/libpostproc \
    $$PWD/externals/ffmpeg/include/libswresample \
    $$PWD/externals/ffmpeg/include/libswscale

DEPENDPATH += $$PWD/externals/ffmpeg/lib

win32:!win32-g++: PRE_TARGETDEPS += \
    $$PWD/externals/ffmpeg/lib/avcodec.lib \
    $$PWD/externals/ffmpeg/lib/avformat.lib \
    $$PWD/externals/ffmpeg/lib/swscale.lib

else:win32-g++: PRE_TARGETDEPS += \
    $$PWD/externals/ffmpeg/lib/libavcodec.dll.a
    $$PWD/externals/ffmpeg/lib/libavformat.dll.a
    $$PWD/externals/ffmpeg/lib/libswscale.dll.a
