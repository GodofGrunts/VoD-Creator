#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <string>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_toolButton_clicked();

    void on_toolButton_2_clicked();

    int on_startHour_textChanged(const QString &arg1);

    int on_startMinute_textChanged(const QString &arg1);

    double on_startSeconds_textChanged(const QString &arg1);

    int on_endHour_textChanged(const QString &arg1);

    int on_endMinute_textChanged(const QString &arg1);

    double on_endSeconds_textChanged(const QString &arg1);

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
